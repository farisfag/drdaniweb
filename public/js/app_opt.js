import "{{public_path(css/app.scss)}}";

// Global
import "{{public_path(js/modules/bootstrap)}}";
import "{{public_path(js/modules/theme)}}";
import "{{public_path(js/modules/feather)}}";
import "{{public_path(js/modules/moment)}}";
import "{{public_path(js/modules/sidebar)}}";
import "{{public_path(js/modules/user-agent)}}";

// Forms
import "{{public_path(js/modules/datetimepicker)}}";

// Charts
import "{{public_path(js/modules/chartjs)}}";

// Maps
import "{{public_path(js/modules/vector-maps)}}";
