<!DOCTYPE html>
<html lang="en">

<head>
    <title>Halaman Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('login_assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('login_assets/fonts/iconic/css/material-design-iconic-font.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/animate/animate.css')}}">
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/css-hamburgers/hamburgers.min.css')}}">
    --}}
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/animsition/css/animsition.min.css')}}">
    --}}
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/select2/select2.min.css')}}"> --}}
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('login_assets/vendor/daterangepicker/daterangepicker.css')}}">
    --}}
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('login_assets/css/main.css')}}">
    <!--===============================================================================================-->
    <style>
        .wrap-input100 {
            border-bottom: 2px solid rgba(0, 175, 145, 0.9);
        }

        .focus-input100::before {
            background: rgba(13, 14, 13, 0.25);;
        }

        .wrap-login100 {
            /* background: #00af91 ; */
            /* color: green !important; */
        }

        .container-login100::before {
            /* background-image:  url('login_assets/images/bg-01.jpg'); */
            background-color: whitesmoke;
        }

        .login100-form-title {
            color: #00af91;
        }

        .input100 {
            color: green;
        }

    </style>
</head>

<body>

    <div class="limiter">
        <div class="container-login100" style="">
            <div class="wrap-login100">
                <form class="login100-form validate-form">
                    {{-- <span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span> --}}

                    <span class="login100-form-title p-b-34 p-t-27">
                        Selamat Datang
                    </span>

                    <div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input class="input100" type="text" name="username" placeholder="Username" autocomplete="off">
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" type="password" name="pass" placeholder="Password">
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    {{-- <div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div> --}}

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    {{-- <div class="text-center p-t-90">
						<a class="txt1" href="#">
							Forgot Password?
						</a>
					</div> --}}
                </form>
            </div>
        </div>
    </div>


    {{-- <div id="dropDownSelect1"></div> --}}

    <!--===============================================================================================-->
    <script src="{{asset('login_assets/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('login_assets/vendor/animsition/js/animsition.min.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('login_assets/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('login_assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--===============================================================================================-->
    {{-- <script src="{{asset('login_assets/vendor/select2/select2.min.js')}}"></script> --}}
    <!--===============================================================================================-->
    {{-- <script src="{{asset('login_assets/vendor/daterangepicker/moment.min.js')}}"></script> --}}
    {{-- <script src="vendor/daterangepicker/daterangepicker.js"></script> --}}
    <!--===============================================================================================-->
    <script src="{{asset('login_assets/vendor/countdowntime/countdowntime.js')}}"></script>
    <!--===============================================================================================-->
    <script src="{{asset('login_assets/js/main.js')}}"></script>

</body>

</html>
