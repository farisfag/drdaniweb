{{-- <script type="text/javascript" src="{{ asset('js/vendor.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>


<script>
    $(document).ready(function () {
        function checkForChanges() {
            if ($('#sidebar').hasClass('collapsed'))
                $('.navbar-custom').css('width', '100%');
            else
                setTimeout(checkForChanges, 500);
                $('.navbar-custom').css('width', 'calc(100% - 260px)');
        }
        // $(checkForChanges);
        // $("p").toggle(
        //     function(){$("p").css({"color": "red"});},
        //     function(){$("p").css({"color": "blue"});}
        // });

        // $(".sidebar-toggle").click(function(){
        //     $(".navbar-custom").toggleClass("navbar-custom-expand")
        // })

        $(".sidebar-toggle").click( function(event){
            event.preventDefault();
            if ( $(this).hasClass("isCollapse") ) {
                $(".custom-expand-name").animate({marginRight:"+=250px"}, 350);
            } else {
                $(".custom-expand-name").animate({marginRight:"-=250px"}, 350);
            }
            $(this).toggleClass("isCollapse");
            return false;
        });
    })

</script>
