<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.headhtml')

    <title>Aplikasi Database Pasien</title>
    <!-- styleextra -->
    @yield('style_extra')

</head>

<body>
    <div class="wrapper">

        @include('include.sidebar')
        <div class="main">

            @include('include.header')

            <main class="content">
                {{-- Begin Content --}}
                @yield('content')
            </main>

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row text-muted">
                        <div class="col-6 text-left">
                            <p class="mb-0">
                                <a href="index.html" class="text-muted"><strong>AdminKit Demo</strong></a> &copy;
                            </p>
                        </div>
                        <div class="col-6 text-right">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#">Support</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#">Help Center</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#">Privacy</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="text-muted" href="#">Terms</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    @include('include.javascript')
    {{-- JS Extra --}}
    @yield('js_extra')


    <script>
        $(function () {
            $('#datetimepicker-dashboard').datetimepicker({
                inline: true,
                sideBySide: false,
                format: 'L'
            });
        });

    </script>

</body>

</html>
