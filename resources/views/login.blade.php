<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.headhtml')

    <title>Login Page</title>

    <style>
        body {
            overflow-y: hidden;
            position: relative;
            /* background-color: #00af91; */
            background-color: #222E3C;
        }

        .form-wrapper {
            width: 500px;
            /* Set this to your convenience */
            position: absolute;
            top: 40%;
            left: 45%;
            margin-top: -100px;
            /* Half of height */
            margin-left: -150px;
            /* Half of width */
        }

        .centered-box {
            /* margin-top: 10vh; */
            width: 100%;
            left: 50%;
            top: 50%;
        }

        .contaner-fluid {
            overflow: hidden;
        }

        .card{
            border-radius: 8px;
            box-shadow: white;
        }

        .card-title {
            font-size: 24px;
            font-weight: 600;
            text-align: center;
        }

        .btn-login {
            width: 100%;
            color: #222E3C;
            background-color: white;
            border: 1px solid #222E3C;
            transition: 0.2s ease-in-out;
            font-size: 16px; 
        }

        .btn-login:hover {
            color: #fff;
            background: #222E3C;
            border: 1px solid transparent;
            transition: 0.2s ease-in-out;
        }

    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="form-wrapper">
            <div class="centered-box">
                <div class="card">
                    <div class="card-header px-5 py-3 mt-2 ">
                        <h5 class="card-title">Selamat Datang</h5>
                    </div>
                    <div class="card-body px-5 pt-0">
                        <form>
                            <div class="form-group">
                                <label class="form-label">Email address</label>
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-primary btn-login my-3">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
